# Functions that calculate snr in higher multipoles. 

from pesummary.utils.utils import logger
import numpy as np

def _mode_array_map(mode_key, approx):
    """Return the mode_array in pycbc format for requested mode

    Parameters
    ----------
    mode_key: str
        Mode key e.g. '22'.
    approx: str
        Waveform approximant.

    Returns
    -------
    mode_array: list
        PyCBC get_fd_waveform appropriate list of lm modes.
    """
    mode_array_dict = {
        "22": [[2, 2], [2, -2]],
        "32": [[3, 2], [3, -2]],
        "21": [[2, 1], [2, -1]],
        "44": [[4, 4], [4, -4]],
        "33": [[3, 3], [3, -3]],
        "43": [[4, 3], [4, -3]],
    }
    # don't include negative m modes in Cardiff Phenom models
    # as will throw an error - they are automatically
    # added by these models. Note: Must include
    # negative m modes for phenomXHM.
    if approx in ["IMRPhenomPv3HM", "IMRPhenomHM"]:
        mode_array_idx = -1
    else:
        mode_array_idx = None

    return mode_array_dict[mode_key][:mode_array_idx]


def _wf_at_detector(hp, hc, fp, fc, dt):
    """Compute the waveform as viewed at the detector
    """
    h = fp * hp.copy() + fc * hc.copy()
    h = h.cyclic_time_shift(dt)
    return h


def _rhosq_hm_perp(
    mass_1,
    mass_2,
    spin_1z,
    spin_2z,
    luminosity_distance,
    iota,
    phase,
    ra,
    dec,
    polarization,
    geocent_time,
    fl,
    fh,
    df,
    psds,
    approx="IMRPhenomXHM",
):
    """Calculate the squared orthogonal optimal SNR in each mode.

    Parameters
    ------
    mass_1: float
    mass_2: float
    spin_1z: float
    spin_2z: float
    luminosity_distance: float
    iota: float
    phase: float
    ra: float
    dec: float
    polarization: float
    geocent_time: float
    fl: float
        Lowest frequency to include in overlap calculations.
    fh: float
        Highest frequency to include in overlap calculations.
    df: float
        Frequency spacing to use in overlap calculations.
    psds: dict
        PyCBC PSD for each detector.

    Returns
    -------
    rhosq: list
        list containing:
            [rhosq_22,
            rhosq_21_perp,
            rhosq_33_perp,
            rhosq_44_perp]
    """
    from pycbc.detector import Detector
    from pycbc.filter import sigmasq, overlap_cplx
    from pycbc.waveform import get_fd_waveform

    mode_keys = ["22", "21", "33", "44"]
    hp, hc = {}, {}
    # geocenter waveforms
    for mode in mode_keys:
        mode_array = _mode_array_map(mode, approx)
        try:
            hp[mode], hc[mode] = get_fd_waveform(
                approximant=approx,
                mass1=mass_1,
                mass2=mass_2,
                delta_f=df,
                f_lower=fl,
                f_final=fh,
                distance=luminosity_distance,
                inclination=iota,
                coa_phase=phase,
                mode_array=mode_array,
                spin1z=spin_1z,
                spin2z=spin_2z
            )
        except RuntimeError:
            params = {
                "approximant": approx,
                "mass1": mass_1,
                "mass2": mass_2,
                "delta_f": df,
                "f_lower": fl,
                "f_final": fh,
                "distance": luminosity_distance,
                "inclination": iota,
                "coa_phase": phase,
                "mode_array": mode_array,
                "spin1z": spin_1z,
                "spin2z": spin_2z,
            }
            raise RuntimeError("Waveform generation failed with params: %s" % params)
    rhosq_22 = 0
    rhosq_hm_perp = {mode: 0 for mode in mode_keys[1:]}
    for det, psd in psds.items():
        # detector waveforms
        ifo = Detector(det)
        # calculate antenna factors and time delay from earth center
        fp, fc = ifo.antenna_pattern(ra, dec, polarization, geocent_time)
        dt = ifo.time_delay_from_earth_center(ra, dec, geocent_time)
        h_22 = _wf_at_detector(hp["22"], hc["22"], fp, fc, dt)
        rhosq_22 += sigmasq(h_22, psd, fl, fh)
        for mode in mode_keys[1:]:
            h_hm = _wf_at_detector(hp[mode], hc[mode], fp, fc, dt)
            rhosq_hm = sigmasq(h_hm, psd, fl, fh)
            # calculate the mode power perpindicular to the 22 mode
            # don't calculate the overlap if SNR ~ 0
            if (rhosq_22<1e-14) or (rhosq_hm<1e-14):
                oo = 0
            else:
                oo = overlap_cplx(h_22, h_hm, psd, fl, fh, normalized=True)
            rhosq_hm_perp[mode] += rhosq_hm * (1 - abs(oo) ** 2)

    return [rhosq_22, rhosq_hm_perp["21"], rhosq_hm_perp["33"], rhosq_hm_perp["44"]]


def higher_mode_orthogonal_optimal_snr(
    mass_1,
    mass_2,
    spin_1z,
    spin_2z,
    luminosity_distance,
    iota,
    phase,
    ra,
    dec,
    polarization,
    geocent_time,
    fl=20,
    fh=2048,
    df=0.125,
    psds={},
    snr=None,
    approx="IMRPhenomXHM",
):
    """Calculate the optimal SNR in each lm mode that is orthogonal to
    the 22 mode. Currently calculated lms: 21, 33, 44.

    Parameters
    ----------
    mass_1: list
    mass_2: list
    spin_1z: list
    spin_2z: list
    luminosity_distance: list
    iota: list
    phase: list
    ra: list
    dec: list
    polarization: list
    geocent_time: list
    fl: float
        Lowest frequency (Hz) to include in overlap calculations.
    fh: float
        Highest frequency (Hz) to include in overlap calculations.
    df: float
        Frequency spacing (Hz) to use in overlap calculations.
    psds: dict
        PyCBC PSD for each detector.
    approx: str
        Waveform approximant to use. Only tested for default: PhenomHM.

    Returns
    -------
    rho_22: array
        Optimal SNR in the 22 mode.
    rho_21_perp: array
        Orthogonal optimal SNR in the 21 mode.
    rho_33_perp: array
        Orthogonal optimal SNR in the 33 mode.
    rho_44_perp: array
        Orthogonal optimal SNR in the 44 mode.

    Notes
    -----
    This code was reviewed for GW190412/GW190814 event papers.
    See https://git.ligo.org/joseph.mills/hm_snr_code_review/
    with hash 92b3b6bb3a0fc031b7ce7801dc9a2d72a7b08b13
    """
    def iterator(length, desc):
        try:
            from tqdm import trange

            return trange(
                length, bar_format=(desc + " | {percentage:3.0f}% | {bar} | {n_fmt}/{total_fmt} " "| {elapsed}")
            )
        except ImportError:
            return range(length)

    if isinstance(fl, (list, np.ndarray)):
        fl = fl[0]

    if psds == {}:
        from pycbc.psd import aLIGOZeroDetHighPower

        logger.warn(
            "No PSD provided. Using 'aLIGOZeroDetHighPower' PSD for "
            "H1 (L1/V1 are omitted) to calculate orthogonal SNR in "
            "higher modes. HM SNR is scaled with optimal_snr provided "
            "in the samples. BEWARE: The shape of the PSD curve can "
            "have an effect on the intrinsic rho_hm. The relative "
            "sensitivity of the detectors also affects rho_hm for "
            "lm modes with l not equal to m. Importantly the 33 and "
            "44 modes will not be affected (beyond the shape of the "
            "noise curve) since both polarizations are scaled by the "
            "same overall factor e.g. \sin\iota for the 33 mode (see "
            "Creighton and Anderson (2011) appendix for orientation "
            "factors)."
        )
        flen = int(fh / df) + 1
        aLIGOpsd = aLIGOZeroDetHighPower(flen, df, fl)
        psds = {"H1": aLIGOpsd}

    _delta_f = list(psds.values())[0].delta_f
    if df != _delta_f:
        logger.warn(
            "Provided PSD has df={} which is different to delta_f specified ({}). "
            "Using the PSD delta_f.".format(_delta_f, df)
        )
        df = _delta_f

    _f_final = list(psds.values())[0].sample_frequencies[-1]
    if fh != _f_final:
        logger.warn(
            "The provided final frequency: {} does not match the final frequency "
            "in the PSD: {}. Using the PSD final frequency.".format(fh, _f_final)
        )
        fh = _f_final

    rhosq_hm_perp = np.array(
        [
            _rhosq_hm_perp(
                mass_1[i],
                mass_2[i],
                spin_1z[i],
                spin_2z[i],
                luminosity_distance[i],
                iota[i],
                phase[i],
                ra[i],
                dec[i],
                polarization[i],
                geocent_time[i],
                fl,
                fh,
                df,
                psds,
                approx=approx,
            )
            for i in iterator(len(mass_1), "calculating rho_hm_perp")
        ]
    )
    rho_hm_perp = np.sqrt(rhosq_hm_perp)
    rho_22, rho_21_perp, rho_33_perp, rho_44_perp = rho_hm_perp.T

    if snr is not None:  # if SNR has been specified scale the sensitivities
        dist_rescaling = snr / rho_22
        rho_22 = snr
        rho_hm = np.array([rho_21_perp, rho_33_perp, rho_44_perp])
        rho_21_perp, rho_33_perp, rho_44_perp = dist_rescaling * rho_hm

    return rho_22, rho_21_perp, rho_33_perp, rho_44_perp

